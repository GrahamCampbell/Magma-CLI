# Magma CLI

## Usage

```bash
$ magma run <filename>
```

## Building

To build the `phar` file, run:

```bash
$ docker run -it -w /data -v ${PWD}:/data:delegated --entrypoint vendor/bin/box --rm registry.gitlab.com/grahamcampbell/php:7.4-base compile
```
