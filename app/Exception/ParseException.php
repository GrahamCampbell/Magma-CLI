<?php

declare(strict_types=1);

namespace GrahamCampbell\Magma\Exception;

use RuntimeException;

class ParseException extends RuntimeException implements MagmaException
{
    //
}
