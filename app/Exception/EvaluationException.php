<?php

declare(strict_types=1);

namespace GrahamCampbell\Magma\Exception;

use RuntimeException;

class EvaluationException extends RuntimeException implements MagmaException
{
    //
}
