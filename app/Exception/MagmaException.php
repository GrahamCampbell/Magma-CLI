<?php

declare(strict_types=1);

namespace GrahamCampbell\Magma\Exception;

use Throwable;

interface MagmaException extends Throwable
{
    //
}
