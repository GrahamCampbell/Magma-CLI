<?php

declare(strict_types=1);

namespace GrahamCampbell\Magma;

use GuzzleHttp\ClientInterface;
use GrahamCampbell\GuzzleFactory\GuzzleFactory;
use GrahamCampbell\Magma\Exception\EvaluationException;
use GrahamCampbell\Magma\Exception\ParseException;
use Psr\Http\Message\StreamInterface;

/**
 * This is the client class.
 *
 * @author Graham Campbell <graham@alt-three.com>
 */
final class Client
{
    /**
     * The guzzle client instance.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $guzzle;

    /**
     * Create a new client instance.
     *
     * @param \GuzzleHttp\ClientInterface $guzzle
     *
     * @return void
     */
    public function __construct(ClientInterface $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * Create a new client using the given host name.
     *
     * @param string $hostName
     *
     * @return self
     */
    public static function create(string $hostName)
    {
        return new self(
            GuzzleFactory::make(['base_uri' => sprintf('https://%s', $hostName), 'timeout' => 150])
        );
    }

    /**
     * Evaluate the given input.
     *
     * @param string $input
     *
     * @throws \GuzzleHttp\Exception\TransferException
     * @throws \GrahamCampbell\Magma\Exception\EvaluationException
     * @throws \GrahamCampbell\Magma\Exception\ParseException
     *
     * @return string
     */
    public function evaluate(string $input)
    {
        $response = $this->guzzle->request('POST', '/xml/calculator.xml', ['form_params' => ['input' => $input]]);

        $data = self::parseXmlStream($response->getBody());

        if (isset($data['headers']['warning'])) {
            if ($data['headers']['warning'] === 'An error occurred. See the output for details.') {
                throw new EvaluationException(self::extractResultString($data));
            } else {
                throw new EvaluationException($data['headers']['warning']);
            }
        }

        return self::extractResultString($data);
    }

    /**
     * Parase an xml stream into an array.
     *
     * @param \Psr\Http\Message\StreamInterface $stream
     *
     * @throws \GrahamCampbell\Magma\Exception\ParseException
     *
     * @return string
     */
    private static function parseXmlStream(StreamInterface $stream)
    {
        $ob = @simplexml_load_string($stream->getContents());

        if ($ob === false) {
            throw new ParseException('The server responded with invalid XML data.');
        }

        return json_decode(json_encode($ob), true);
    }

    /**
     * Extract the result string from the array.
     *
     * @param array $data
     *
     * @throws \GrahamCampbell\Magma\Exception\ParseException
     *
     * @return string
     */
    private static function extractResultString(array $data)
    {
        $output = '';

        if (isset($data['results']['line'])) {
            foreach ($data['results']['line'] as $line) {
                if (is_string($line)) {
                    $output .= "{$line}\n";
                }
            }
        } else {
            throw new ParseException('The server did not provide a result.');
        }

        return $output;
    }
}
