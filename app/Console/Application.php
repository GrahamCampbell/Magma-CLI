<?php

declare(strict_types=1);

namespace GrahamCampbell\Magma\Console;

use GrahamCampbell\Magma\Client;
use GrahamCampbell\Magma\Console\Commands\RunCommand;
use Symfony\Component\Console\Application as SymfonyApplication;

final class Application extends SymfonyApplication
{
    /**
     * The application name.
     *
     * @var string
     */
    const APP_NAME = 'Magma';

    /**
     * The application version.
     *
     * @var string
     */
    const APP_VERSION = '1.0.0';

    /**
     * The API host name.
     *
     * @var string
     */
    const API_HOST = 'magma.maths.usyd.edu.au';

    /**
     * Create a new Magma CLI application.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(self::APP_NAME, self::APP_VERSION);
        $this->add(new RunCommand(Client::create(self::API_HOST)));
    }
}
