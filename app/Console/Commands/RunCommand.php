<?php

declare(strict_types=1);

namespace GrahamCampbell\Magma\Console\Commands;

use GrahamCampbell\Magma\Client;
use GrahamCampbell\Magma\Exception\EvaluationException;
use GrahamCampbell\Magma\Exception\ParseException;
use GuzzleHttp\Exception\TransferException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
/**
 * This is the run command class.
 *
 * @author Graham Campbell <graham@alt-three.com>
 */
final class RunCommand extends Command
{
    /**
     * The client instance.
     *
     * @var \GrahamCampbell\Magma\Client
     */
    private $client;

    /**
     * Create a new run command instance.
     *
     * @param \GrahamCampbell\Magma\Client $client
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        parent::__construct();
    }

    /**
     * Configures the command.
     *
     * This method is called by the parent's constructor.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('run');
        $this->setDescription('Execute magma on the given input');
        $this->addArgument('filename', InputArgument::OPTIONAL, 'The input filename');
    }

    /**
     * Executes the command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->write(
                $this->client->evaluate(self::getInputContent($input)),
                OutputInterface::VERBOSITY_QUIET
            );
        } catch (TransferException|ParseException $e) {
            throw new RuntimeException('There was a problem communicating with the server.', 0, $e);
        } catch (EvaluationException $e) {
            throw new RuntimeException('There was a problem with the input program or computation.', 0, $e);
        }

        return 0;
    }

    /**
     * Get the command input content.
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     *
     * @return string
     */
    private static function getInputContent(InputInterface $input)
    {
        if ($filename = $input->getArgument('filename')) {
            $contents = @file_get_contents($filename);
        } elseif (0 === ftell(STDIN)) {
            $contents = '';
            while (!feof(STDIN)) {
                $contents .= fread(STDIN, 1024);
            }
        } else {
            $contents = null;
        }

        if (!is_string($contents)) {
            throw new RuntimeException('Please provide a filename or pipe content to STDIN.');
        }

        return $contents;
    }
}
