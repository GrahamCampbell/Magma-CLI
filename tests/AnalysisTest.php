<?php

declare(strict_types=1);

namespace GrahamCampbell\Tests\Magma;

use GrahamCampbell\Analyzer\AnalysisTrait;
use PHPUnit\Framework\TestCase;

class AnalysisTest extends TestCase
{
    use AnalysisTrait;

    /**
     * Get the code paths to analyze.
     *
     * @return string[]
     */
    protected function getPaths()
    {
        return [
            realpath(__DIR__.'/../bin'),
            realpath(__DIR__.'/../app'),
            realpath(__DIR__),
        ];
    }
}
