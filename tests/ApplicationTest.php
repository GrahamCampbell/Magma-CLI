<?php

declare(strict_types=1);

namespace GrahamCampbell\Tests\Magma;

use GrahamCampbell\Magma\Console\Application;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application as SymfonyApplication;

class ApplicationTest extends TestCase
{
    public function testCreateApplication()
    {
        $app = new Application();

        $this->assertInstanceOf(SymfonyApplication::class, $app);
        $this->assertSame('Magma', $app->getName());
    }
}
